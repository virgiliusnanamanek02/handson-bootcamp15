# Laundry Service

* **Customer**
    * Id
    * First name
    * Last name
    * Address
	    * Id
	    * Street
	    * District
	    * City
	    * Province
	    * Country
    * Phone
    * Email
    * Avatar
   
 * **Employee**
    * Id
    * Name
    * Birth Date
    * Address
	    * Id
	    * Street
	    * District
	    * City
	    * Province
	    * Country
	    * Longitude
	    * Latitude
    * Phone
    * Email
* **Order**
    * Id
    * Order Items
	    * Id
	    * Item Name
	    * Weight
	    * Amount
	    * Service Id
    * Order date
    * Status (new, picked up, processing, delivered, finished)
    * Payment status (unpaid, paid)
    * Last update date
    * Update by
* **Service**
    * Id
    * Name (washing, ironing)
    * Price
    * Unit (weight/piece)
    * Type (regular/express)


## Endpoint untuk REST API Laundry Service


**Customer:**

* **GET /customers:** Mendapatkan semua customer.
* **GET /customers/{id}:** Mendapatkan customer dengan ID tertentu.
* **POST /customers:** Membuat customer baru.
* **PUT /customers/{id}:** Memperbarui customer dengan ID tertentu.
* **DELETE /customers/{id}:** Menghapus customer dengan ID tertentu.

**Address (Nested within Customer and Employee):**

* **GET /customers/{customerId}/address:** Mendapatkan alamat customer dengan ID tertentu.
* **GET /employees/{employeeId}/address:** Mendapatkan alamat employee dengan ID tertentu.
* **PUT /customers/{customerId}/address:** Memperbarui alamat customer dengan ID tertentu.
* **PUT /employees/{employeeId}/address:** Memperbarui alamat employee dengan ID tertentu.

**Employee:**

* **GET /employees:** Mendapatkan semua employee.
* **GET /employees/{id}:** Mendapatkan employee dengan ID tertentu.
* **POST /employees:** Membuat employee baru.
* **PUT /employees/{id}:** Memperbarui employee dengan ID tertentu.
* **DELETE /employees/{id}:** Menghapus employee dengan ID tertentu.

**Order:**

* **GET /orders:** Mendapatkan semua order.
* **GET /orders/{id}:** Mendapatkan order dengan ID tertentu.
* **POST /orders:** Membuat order baru.
* **PUT /orders/{id}:** Memperbarui order dengan ID tertentu.
* **DELETE /orders/{id}:** Menghapus order dengan ID tertentu.
* **PATCH /orders/{id}/status:** Memperbarui status order dengan ID tertentu.
* **PATCH /orders/{id}/payment-status:** Memperbarui status pembayaran order dengan ID tertentu.

**Order Item (Nested within Order):**

* **GET /orders/{orderId}/items:** Mendapatkan semua item order dengan order ID tertentu.
* **GET /orders/{orderId}/items/{itemId}:** Mendapatkan item order dengan order ID dan item ID tertentu.
* **POST /orders/{orderId}/items:** Menambahkan item ke order dengan order ID tertentu.
* **PUT /orders/{orderId}/items/{itemId}:** Memperbarui item order dengan order ID dan item ID tertentu.
* **DELETE /orders/{orderId}/items/{itemId}:** Menghapus item order dengan order ID dan item ID tertentu.

**Service:**

* **GET /services:** Mendapatkan semua service.
* **GET /services/{id}:** Mendapatkan service dengan ID tertentu.
* **POST /services:** Membuat service baru.
* **PUT /services/{id}:** Memperbarui service dengan ID tertentu.
* **DELETE /services/{id}:** Menghapus service dengan ID tertentu.



